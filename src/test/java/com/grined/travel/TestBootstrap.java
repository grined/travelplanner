package com.grined.travel;

import com.github.fakemongo.Fongo;
import com.grined.travel.entity.User;
import com.grined.travel.repository.UserRepository;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.BasicMongoPersistentEntity;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MappingMongoEntityInformation;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;
import org.springframework.data.util.ClassTypeInformation;
import org.springframework.stereotype.Controller;
import org.springframework.test.annotation.DirtiesContext;

import java.io.Serializable;

@Configuration
@ComponentScan(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
        value = Bootstrap.class))
@EnableMongoRepositories
public class TestBootstrap extends AbstractMongoConfiguration {
    @Override
    protected String getDatabaseName() {
        return "travel";
    }

    @Bean
    public Mongo mongo() throws Exception {
        return new Fongo("mongo-test").getMongo();
    }
}