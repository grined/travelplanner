package com.grined.travel;


import com.grined.travel.dto.CreateAccountDto;
import com.grined.travel.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBootstrap.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public void testThatCantCreateUserWithNullDto(){
        assertFalse(userService.createNewAccount(null));
    }

    @Test
    public void testThatCantCreateUserWithoutUsername(){
        CreateAccountDto createAccountDto = new CreateAccountDto();
        createAccountDto.setUsername("");
        createAccountDto.setPassword("1");
        createAccountDto.setCpassword("1");
        assertFalse(userService.createNewAccount(createAccountDto));
    }

    @Test
    public void testThatCantCreateUserWithoutPassword(){
        CreateAccountDto createAccountDto = new CreateAccountDto();
        createAccountDto.setUsername("");
        createAccountDto.setPassword("");
        createAccountDto.setCpassword("1");
        assertFalse(userService.createNewAccount(createAccountDto));
    }

    @Test
    public void testThatCantCreateUserWithBadPasswordAndPasswordConfirmation(){
        CreateAccountDto createAccountDto = new CreateAccountDto();
        createAccountDto.setUsername("1");
        createAccountDto.setPassword("1");
        createAccountDto.setCpassword("12");
        assertFalse(userService.createNewAccount(createAccountDto));
    }

    @Test
    public void testThatCanCreateUserWithCorrectInformation(){
        CreateAccountDto createAccountDto = new CreateAccountDto();
        createAccountDto.setUsername("1");
        createAccountDto.setPassword("1");
        createAccountDto.setCpassword("1");
        assertTrue(userService.createNewAccount(createAccountDto));
    }

    @Test
    public void testThatCantCreateTwoUsersWithSameName(){
        CreateAccountDto createAccountDto1 = new CreateAccountDto();
        createAccountDto1.setUsername("1");
        createAccountDto1.setPassword("1");
        createAccountDto1.setCpassword("1");
        CreateAccountDto createAccountDto2 = new CreateAccountDto();
        createAccountDto2.setUsername("1");
        createAccountDto2.setPassword("1");
        createAccountDto2.setCpassword("1");
        boolean newAccount = userService.createNewAccount(createAccountDto1);
        assertTrue(!userService.createNewAccount(createAccountDto2) && newAccount);
    }
}
