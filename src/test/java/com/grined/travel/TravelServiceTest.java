package com.grined.travel;


import com.grined.travel.dto.CreateAccountDto;
import com.grined.travel.dto.TravelDto;
import com.grined.travel.entity.Travel;
import com.grined.travel.exception.CustomException;
import com.grined.travel.exception.DateException;
import com.grined.travel.exception.WrongIdException;
import com.grined.travel.service.TravelService;
import com.grined.travel.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBootstrap.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TravelServiceTest {
    @Autowired
    private TravelService travelService;
    @Autowired
    private UserService userService;

    private String createUser(String username){
        CreateAccountDto createAccountDto = new CreateAccountDto();
        createAccountDto.setUsername(username);
        createAccountDto.setPassword("1");
        createAccountDto.setCpassword("1");
        userService.createNewAccount(createAccountDto);
        return username;
    }

    @Test(expected = DateException.class)
    public void testThatNewCantAddTravelWithWrongDateFields() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("02/02/2015");
        travelService.addNewTravel(travelDto, username);
    }

    @Test(expected = DateException.class)
    public void testThatNewCantAddTravelWithEndDateIsEarlierThanStartDate() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("01-01-2014");
        travelService.addNewTravel(travelDto, username);
    }

    @Test
    public void testThatSuccessfulReturnNoTravels() {
        String username = createUser("1");
        assertTrue(travelService.findTravels(username).isEmpty());
    }

    @Test
    public void testThatSuccessfulCreateTravel() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("02-02-2015");
        Travel travel = travelService.addNewTravel(travelDto, username);
        assertEquals(travelService.findTravels(username).get(0).getTravelId(), travel.getId());
    }

    @Test
    public void testThatFindOnlyTravelsBySpecificUser() throws CustomException {
        String username1 = createUser("1");
        String username2 = createUser("2");
        TravelDto travelDto1 = new TravelDto();
        travelDto1.setStartDate("01-01-2015");
        travelDto1.setEndDate("02-02-2015");
        TravelDto travelDto2 = new TravelDto();
        travelDto2.setStartDate("01-01-2015");
        travelDto2.setEndDate("02-02-2015");

        Travel travel1 = travelService.addNewTravel(travelDto1, username1);
        Travel travel2 = travelService.addNewTravel(travelDto2, username2);

        List<TravelDto> findingTravels = travelService.findTravels(username1);
        assertEquals(1, findingTravels.size());
        assertEquals(travel1.getId(), findingTravels.get(0).getTravelId());
    }

    @Test
    public void testThatCorrectlyFindNextMonthTravels() throws CustomException {
        String username1 = createUser("1");
        TravelDto travelDto1 = new TravelDto();
        travelDto1.setStartDate("01-01-2000");
        travelDto1.setEndDate("02-02-2015");
        TravelDto travelDto2 = new TravelDto();
        travelDto2.setStartDate("01-01-2000");
        travelDto2.setEndDate("02-02-2015");
        TravelDto travelDto3 = new TravelDto();
        LocalDate instant = LocalDate.now().plusMonths(1);
        travelDto3.setStartDate(TravelService.FORMATTER.format(instant));
        travelDto3.setEndDate(TravelService.FORMATTER.format(instant.plusDays(20)));

        Travel travel1 = travelService.addNewTravel(travelDto1, username1);
        Travel travel2 = travelService.addNewTravel(travelDto2, username1);
        Travel travel3 = travelService.addNewTravel(travelDto3, username1);

        List<TravelDto> findingTravels = travelService.findTravels(username1, true);
        assertEquals(1, findingTravels.size());
        assertEquals(travel3.getId(), findingTravels.get(0).getTravelId());
    }

    @Test(expected = WrongIdException.class)
    public void testThatCantEditTravelWithBadId() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("01-01-2018");
        Travel addedTravel = travelService.addNewTravel(travelDto, username);
        TravelDto editDto = new TravelDto();
        editDto.setTravelId(addedTravel.getId()+"someText");
        editDto.setStartDate("01-01-2015");
        editDto.setEndDate("01-02-2018");
        travelService.editTravel(editDto);
    }

    @Test
    public void testThatCorrectlyEditTravel() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("01-01-2018");
        Travel addedTravel = travelService.addNewTravel(travelDto, username);
        TravelDto editDto = new TravelDto();
        String description = "Descr";
        editDto.setTravelId(addedTravel.getId());
        editDto.setDescription(description);
        editDto.setStartDate("01-01-2015");
        editDto.setEndDate("01-02-2018");
        travelService.editTravel(editDto);
        assertEquals(description, travelService.findTravels(username).get(0).getDescription());
    }

    @Test(expected = WrongIdException.class)
    public void testThatCantDeleteTravelWithBadId() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("01-01-2018");
        Travel addedTravel = travelService.addNewTravel(travelDto, username);
        travelService.deleteTravel(addedTravel.getId() + "someText");
    }

    @Test
    public void testThatCorrectlyDeleteTravel() throws CustomException {
        String username = createUser("1");
        TravelDto travelDto = new TravelDto();
        travelDto.setStartDate("01-01-2015");
        travelDto.setEndDate("01-01-2018");
        Travel addedTravel = travelService.addNewTravel(travelDto, username);
        travelService.deleteTravel(addedTravel.getId());
        assertEquals(0, travelService.findTravels(username).size());
    }

}
