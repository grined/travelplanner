package com.grined.travel.controller;

import com.grined.travel.dto.CreateAccountDto;
import com.grined.travel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

@Controller
public class MainController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/createAccount", method = RequestMethod.POST)
    @ResponseBody
    private Boolean createAccount(@Valid @RequestBody CreateAccountDto createAccountDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            return false;
        }
        return userService.createNewAccount(createAccountDto);
    }
}
