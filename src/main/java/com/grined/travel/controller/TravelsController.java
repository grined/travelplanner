package com.grined.travel.controller;

import com.grined.travel.dto.RequestResultDto;
import com.grined.travel.dto.TravelDto;
import com.grined.travel.exception.CustomException;
import com.grined.travel.exception.WrongIdException;
import com.grined.travel.service.TravelService;
import com.grined.travel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@Secured(UserService.ROLE_USER)
@RequestMapping(value = "/travels")
public class TravelsController {
    @Autowired
    private TravelService travelService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    private List<TravelDto> findTravels() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return travelService.findTravels(username);
    }

    @RequestMapping(value = "/nextMonth", method = RequestMethod.GET)
    @ResponseBody
    private List<TravelDto> findTravelsForNextMonth() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return travelService.findTravels(username, true);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    private RequestResultDto addTravel(@Valid @RequestBody TravelDto travelDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            return new RequestResultDto(true, "Bad input data");
        }
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            travelService.addNewTravel(travelDto, username);
        } catch (CustomException e) {
            return new RequestResultDto(true, e.getMessage());
        }
        return new RequestResultDto(false, "Successfully added");
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    private RequestResultDto editTravel(@Valid @RequestBody TravelDto travelDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            return new RequestResultDto(true, "Bad input data");
        }
        try {
            travelService.editTravel(travelDto);
        } catch (CustomException e) {
            return new RequestResultDto(true, e.getMessage());
        }
        return new RequestResultDto(false, "Successfully edited");
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    private RequestResultDto deleteTravel(@RequestParam String travelId) {
        try {
             travelService.deleteTravel(travelId);
        } catch (WrongIdException e) {
            return new RequestResultDto(true, e.getMessage());
        }
        return new RequestResultDto(false, "Successfully deleted");
    }

}
