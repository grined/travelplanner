package com.grined.travel.repository;

import com.grined.travel.entity.Travel;
import com.grined.travel.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TravelRepository extends MongoRepository<Travel, String> {
    List<Travel> findByUserId(String userId);
}
