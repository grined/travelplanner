package com.grined.travel.dto;

public class RequestResultDto {
    private boolean hasError;
    private String resultMsg;

    public RequestResultDto() {
    }

    public RequestResultDto(boolean hasError, String resultMsg) {
        this.hasError = hasError;
        this.resultMsg = resultMsg;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
}
