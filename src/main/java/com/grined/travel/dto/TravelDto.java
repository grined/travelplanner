package com.grined.travel.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TravelDto {
    private String travelId;
    private int daysLeft;
    @NotNull
    @Size(min=1)
    private String destination;
    @NotNull
    @Size(min=1)
    private String startDate;
    @NotNull
    @Size(min=1)
    private String endDate;
    @NotNull
    private String description;

    public String getTravelId() {
        return travelId;
    }

    public void setTravelId(String travelId) {
        this.travelId = travelId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft;
    }
}
