package com.grined.travel.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateAccountDto {
    @NotNull
    @Size(min=1)
    private String username;
    @NotNull
    @Size(min=1)
    private String password;
    @NotNull
    @Size(min=1)
    private String cpassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }
}
