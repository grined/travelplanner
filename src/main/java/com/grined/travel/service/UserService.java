package com.grined.travel.service;

import com.grined.travel.dto.CreateAccountDto;
import com.grined.travel.entity.Travel;
import com.grined.travel.entity.User;
import com.grined.travel.repository.TravelRepository;
import com.grined.travel.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class UserService implements UserDetailsService, PasswordEncoder {
    public static final String ROLE_USER = "ROLE_USER";
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        final User user = userRepository.findByUsername(login);
        if (user == null){
            throw new UsernameNotFoundException(login);
        }
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                List<SimpleGrantedAuthority> auths = new ArrayList<>();
                auths.add(new SimpleGrantedAuthority(ROLE_USER));
                return auths;
            }

            @Override
            public String getPassword() {
                return user.getPassword();
            }

            @Override
            public String getUsername() {
                return user.getUsername();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }

    public boolean createNewAccount(CreateAccountDto createAccountDto){
        if (createAccountDto!= null &&
                StringUtils.equals(createAccountDto.getPassword(), createAccountDto.getCpassword()) &&
                StringUtils.isNotEmpty(createAccountDto.getPassword()) &&
                StringUtils.isNotEmpty(createAccountDto.getUsername()) &&
                userRepository.findByUsername(createAccountDto.getUsername()) == null) {
            User user = new User();
            user.setUsername(createAccountDto.getUsername());
            user.setPassword(encode(createAccountDto.getPassword()));
            if (userRepository.save(user) != null){
                return true;
            }
        }
        return false;
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return DigestUtils.md5DigestAsHex((rawPassword.toString() + "sdyhbce456!@#$").getBytes());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(encode(rawPassword));
    }
}
