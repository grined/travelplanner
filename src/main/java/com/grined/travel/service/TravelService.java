package com.grined.travel.service;

import com.grined.travel.dto.TravelDto;
import com.grined.travel.entity.Travel;
import com.grined.travel.entity.User;
import com.grined.travel.exception.CustomException;
import com.grined.travel.exception.DateException;
import com.grined.travel.exception.WrongIdException;
import com.grined.travel.repository.TravelRepository;
import com.grined.travel.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TravelService {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy");
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TravelRepository travelRepository;

    public List<TravelDto> findTravels(String username) {
        return findTravels(username, false);
    }

    public List<TravelDto> findTravels(String username, boolean onlyNextMonth) {
        User user = userRepository.findByUsername(username);
        return travelRepository.findByUserId(user.getId())
                .stream()
                .filter(t -> !onlyNextMonth ||
                        YearMonth.from(DateUtils.fromDate(t.getStartDate())).equals(YearMonth.now().plusMonths(1)))
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public Travel addNewTravel(TravelDto travelDto, String username) throws CustomException {
        Travel travel = fromDto(travelDto);
        User user = userRepository.findByUsername(username);
        travel.setUserId(user.getId());
        return travelRepository.save(travel);
    }

    public Travel editTravel(TravelDto travelDto) throws CustomException {
        if (StringUtils.isNotEmpty(travelDto.getTravelId())) {
            Travel travel = travelRepository.findOne(travelDto.getTravelId());
            if (travel != null){
                Travel edited = fromDto(travelDto);
                edited.setId(travel.getId());
                edited.setUserId(travel.getUserId());
                return travelRepository.save(edited);
            }
        }
        throw new WrongIdException("Can not find such travel");
    }

    public void deleteTravel(String travelId) throws WrongIdException {
        if (StringUtils.isNotEmpty(travelId)) {
            Travel travel = travelRepository.findOne(travelId);
            if (travel != null){
                travelRepository.delete(travel.getId());
                return;
            }
        }
        throw new WrongIdException("Can not find such travel");
    }

    private Travel fromDto(TravelDto travelDto) throws DateException{
        Travel travel = new Travel();
        travel.setDestination(travelDto.getDestination());
        travel.setDescription(travelDto.getDescription());
        try {
            travel.setStartDate(DateUtils.toDate(LocalDate.parse(travelDto.getStartDate(), FORMATTER)));
            travel.setEndDate(DateUtils.toDate(LocalDate.parse(travelDto.getEndDate(), FORMATTER)));
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            throw new DateException("Illegal date format");
        }
        if (ChronoUnit.DAYS.between(
                DateUtils.fromDate(travel.getStartDate()),
                DateUtils.fromDate(travel.getEndDate())) < 0){
            throw new DateException("End date is less then start date");
        }
        return travel;
    }

    private TravelDto toDto(Travel travel){
        TravelDto travelDto = new TravelDto();
        travelDto.setDestination(travel.getDestination());
        travelDto.setDescription(travel.getDescription());
        travelDto.setStartDate(FORMATTER.format(DateUtils.fromDate(travel.getStartDate())));
        travelDto.setEndDate(FORMATTER.format(DateUtils.fromDate(travel.getEndDate())));
        travelDto.setTravelId(travel.getId());
        long between = ChronoUnit.DAYS.between(LocalDate.now(), DateUtils.fromDate(travel.getStartDate()));
        travelDto.setDaysLeft(Long.valueOf(between).intValue());
        return travelDto;
    }
}
