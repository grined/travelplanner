package com.grined.travel.exception;

public class WrongIdException extends CustomException {
    public WrongIdException(String message) {
        super(message);
    }
}
