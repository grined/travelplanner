package com.grined.travel.exception;

public class DateException extends CustomException {
    public DateException(String message) {
        super(message);
    }
}
