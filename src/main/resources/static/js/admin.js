angular.module('admin', [])
    .controller('AccountCtrl', function ($scope, $http, $location) {
        $scope.showAccountCreation = false;
        $scope.accountCreationMsg = "Create new account";
        $scope.accountCreatingShowInfo = false;
        $scope.accountCreatingMsg = "";

        $scope.createNewAccount = function(){
            $http.post("/createAccount",
                {   username:$scope.new_username,
                    password:$scope.new_password,
                    cpassword:$scope.new_c_password})
                .success(function(data){
                    if (data == 'true'){
                        $scope.accountCreatingShowInfo = true;
                        $scope.accountCreatingMsg = "Creation success";
                        $scope.switchCreateAccount();
                    } else {
                        $scope.accountCreatingShowInfo = true;
                        $scope.accountCreatingMsg = "Cannot create such account =(";
                    }
                })
                .error(function(){
                    $scope.accountCreatingShowInfo = true;
                    $scope.accountCreatingMsg = "Cannot create such account =(";
                })
        }
        $scope.switchCreateAccount = function(){
            $scope.showAccountCreation = !$scope.showAccountCreation;
            if ($scope.showAccountCreation){
                $scope.accountCreationMsg = "Hide account creation";
                $scope.accountCreatingShowInfo = false;
                $scope.accountCreatingMsg = "";
            } else {
                $scope.accountCreationMsg = "Create new account"
            }
        }
        $scope.isLoginError = function(){
            return $location.absUrl().indexOf("?error") > -1;
        };

    })
;


