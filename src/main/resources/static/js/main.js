var app = angular.module('travel', ['ngTable', 'mgcrea.ngStrap'])
    .controller('MainCtrl', function ($http, $scope, $filter, ngTableParams) {
        var data = [];
        var defaultTravel ={description:""};

        $scope.selectedTravel = {description:""};
        $scope.operationResult = {resultMsg : "Last operation result"}
        $scope.badResult = {hasError: true, resultMsg : "Internal error"}

        $scope.cloneJSON = function(json){
            return JSON.parse(JSON.stringify(json));
        }

        $scope.applyCallback = function(callback) {
            $scope.operationResult = callback;
            if ($scope.operationResult.hasError == false) {
                $scope.selectedTravel = $scope.cloneJSON(defaultTravel);
                $scope.getAllTravels();
            }
        }

        $scope.getAllTravels = function() {
            var str = $scope.checkboxValue ? "/nextMonth" : "";
            $http.get("/travels"+str)
                .success(function(result) {
                    data = result;
                    $scope.tableParams.reload();
                    if (str == "/nextMonth"){
                        $scope.tableParams.page(1);
                    }
                })
                .error(function(){
                    $scope.operationResult = $scope.cloneJSON($scope.badResult);
                })
        };

        $scope.addNewTravel = function() {
            $http.post("/travels", $scope.selectedTravel)
                .success(function(callback) {
                    $scope.applyCallback(callback);
                })
                .error(function(){
                    $scope.operationResult = $scope.cloneJSON($scope.badResult);
                })

        };

        $scope.deleteTravel = function() {
            $http.delete("/travels", {params: {travelId: $scope.selectedTravel.travelId}})
                .success(function(callback) {
                    $scope.applyCallback(callback);
                })
                .error(function(){
                    $scope.operationResult = $scope.cloneJSON($scope.badResult);
                })

        };

        $scope.editTravel = function() {
            $http.put("/travels", $scope.selectedTravel)
                .success(function(callback) {
                    $scope.applyCallback(callback);
                })
                .error(function(){
                    $scope.operationResult = $scope.cloneJSON($scope.badResult);
                })

        };

        $scope.$watch('checkboxValue', function(){
            $scope.getAllTravels();
        })

        $scope.resetSelection = function() {
            for (var i = 0; i < data.length; i++) {
                data[i].$selected = false;
            }
            $scope.selectedTravel = $scope.cloneJSON(defaultTravel);
        };


        $scope.selectionChanged = function(travel) {
            travel.$selected = true;
            $scope.selectedTravel = $scope.cloneJSON(travel);
        }

        var filterValue;

        $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 15,          // count per page
            filter: {
            },
            sorting: {
                daysLeft : 'asc'
            }
        }, {
            counts: [], // hide page counts control
            total:   data.length, // length of data
            getData: function ($defer, params) {
                // use build-in angular filter
                if (JSON.stringify(params.filter()) !== filterValue){
                    filterValue = JSON.stringify(params.filter());
                    params.page(1);
                }
                var filteredData = params.filter() ?
                    $filter('filter')(data, params.filter()) :
                    data;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    data;

                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            },
            $scope: { $data: {} }
        });

        $scope.getAllTravels();

    });


