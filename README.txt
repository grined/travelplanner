Simple travel planner

- User must be able to create an account and log in

- When logged in, user can see, edit and delete trips he entered

- When entered, each trip has Destination, StartDate, EndDate, Comment

- When displayed, each entry has also day count to trip start (only for future trips)

- User can filter trips

- Print travel plan for next month

- REST API. Make it possible to perform all user actions via the API, including authentication (If a mobile application and you don’t know how to create your own backend you can use Parse.com, Firebase.com or similar services to create the API).

- All actions need to be done client side using AJAX, refreshing the page is not acceptable. (If a mobile app, disregard this)

- In any case you should be able to explain how a REST API works and demonstrate that by creating functional tests that use the REST Layer directly.

- Bonus: unit tests!

- You will not be marked on graphic design, however, do try to keep it as tidy as possible.
-----------------------------------------------------------------------------------------------------------------------

It is client-server application for web.
Client part is write on html5 using javascript and such frameworks as AngularJS and Twitter bootstrap.
Server-side is write on java 1.8 with using Spring framework and such components of Spring as Security, Boot, Data,
MVC and standard DI.
MongoDB is selected as database for this application.
Connection between server-side and MongoDB is implemented by Spring Data.
Connection between client and server-side is implemented by using REST.
Application is built using gradle.

To run application - run 'gradle run' in directory where build.gradle file is situated.
